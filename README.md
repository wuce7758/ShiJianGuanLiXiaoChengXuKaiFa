前言

以前看过《把时间当作朋友》这本书，里面有个管理时间的观点个人觉得很有用，就是养成记录自己每天的时间消耗的一个习惯，这样有助于更有效地管理自己的时间。但是个人觉得每天在笔记本上记录时间有点麻烦，就写一个简单的小程序来记录自己的时间。

项目介绍

简要介绍： 一个时间管理的小程序，适合新手 入门学习 。

主要功能： 制定每天的时间计划，记录今天的时间消耗。

技术构成： wxss + wxml + js + node.js + mongodb

github地址： FatDong1的github

项目截图

制定明天的时间计划

![输入图片说明](https://git.oschina.net/uploads/images/2017/0802/094816_4bdf6b28_1392442.png "1.png")

记录今天的时间使用

左边的评分按钮可以把今天的时间消耗和昨天制定的计划进行比较，然后通过一点简单算法计算出分数。星星的颗数代表不同的权重。

![输入图片说明](https://git.oschina.net/uploads/images/2017/0802/094841_deeeb299_1392442.png "2.png")

自定义时间计划模板

![输入图片说明](https://git.oschina.net/uploads/images/2017/0802/094902_67ecc3d5_1392442.png "3.png")

设置奖励

达到自己设定的条件后，会弹出奖励页面的模态框。

![输入图片说明](https://git.oschina.net/uploads/images/2017/0802/094921_aa03ab0b_1392442.png "4.png")

![输入图片说明](https://git.oschina.net/uploads/images/2017/0802/094940_28093f2f_1392442.png "5.png")


数据统计、评分

与github的contribution类似，分数越高，星星越绿。底下的火焰个数代表奖励实现的次数。

![输入图片说明](https://git.oschina.net/uploads/images/2017/0802/094957_20b670ad_1392442.png "6.png")

排行榜

获取用户的唯一标识openid、头像、昵称

![输入图片说明](https://git.oschina.net/uploads/images/2017/0802/095054_53ae9160_1392442.png "7.png")

最后

由于小程序暂时不允许发布具有备忘录类功能的项目，所以这个小程序也就没有通过审核:sob::sob::sob::sob:各位小伙伴如果抱着发布个小程序的想法，一定要先看看小程序的官方介绍呀，感觉花了2个星期还不能发布有点坑。

想要更多小程序资源代码，扫码关注第九程序公众号：

![输入图片说明](https://git.oschina.net/uploads/images/2017/0802/095145_fc8bb10a_1392442.jpeg "qrcode_for_gh_812f6af9d7ca_258 (1).jpg")